<?php

namespace common\models;

use Yii;

class Dosen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nid', 'nama', 'jenis_kelamin', 'id_matakuliah'], 'required'],
            [['nama'], 'string'],
            [['nid', 'id_matakuliah'], 'string', 'max' => 100],
            [['jenis_kelamin'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nid' => 'Nid',
            'nama' => 'Nama',
            'jenis_kelamin' => 'Jenis Kelamin',
            'id_matakuliah' => 'Id Matakuliah',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['unique_id' => 'nid']);
    }
}
