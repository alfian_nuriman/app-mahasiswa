<?php

namespace common\models;

use Yii;

class Matakuliah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matakuliah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_matakuliah', 'nama'], 'required'],
            [['nama'], 'string'],
            [['id_matakuliah'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_matakuliah' => 'Id Matakuliah',
            'nama' => 'Nama',
        ];
    }

    public function getSks()
    {
        return $this->hasOne(Matakuliah::className(), ['id_matakuliah' => 'id_matakuliah']);
    }
}
