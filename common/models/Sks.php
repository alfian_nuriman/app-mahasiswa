<?php

namespace common\models;

use Yii;

class Sks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_matakuliah'], 'required'],
            [['nim'], 'string'],
            [['id_sks', 'id_matakuliah', 'nid'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_sks' => 'Id Sks',
            'id_matakuliah' => 'Id Matakuliah',
            'nid' => 'Nid',
            'nim' => 'Nim',
        ];
    }

    public function getMatakuliah()
    {
        return $this->hasOne(Matakuliah::className(), ['id_matakuliah' => 'id_matakuliah']);
    }

    public function canRegister($nim)
    {
        $sks = $this->find()
            ->where(['nim' => $nim])
            ->count();

        if ($sks < 24) {
            return true;
        } else {
            return false;
        }
    }

    public function generateCode()
    {
        $count = $this->find()->count();

        switch ($count) {
            case $count < 10:
                $res = '00000' . $count;
                break;

            case $count < 100:
                $res = '0000' . $count;
                break;

            case $count < 1000:
                $res = '000' . $count;
                break;

            case $count < 10000:
                $res = '00' . $count;
                break;

            case $count < 100000:
                $res = '0' . $count;
                break;

            case 0:
                $res = '00000' . $count;
                break;
            
            default:
                $res = '999999' . $count;
                break;
        }

        return $res;
    }
}
