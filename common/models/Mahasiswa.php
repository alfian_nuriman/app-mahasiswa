<?php

namespace common\models;

use Yii;

class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nim', 'nama', 'jenis_kelamin', 'jurusan', 'semster'], 'required'],
            [['jurusan'], 'string'],
            [['semster'], 'integer'],
            [['nim'], 'string', 'max' => 100],
            [['nama'], 'string', 'max' => 255],
            [['jenis_kelamin'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nim' => 'Nim',
            'nama' => 'Nama',
            'jenis_kelamin' => 'Jenis Kelamin',
            'jurusan' => 'Jurusan',
            'semster' => 'Semster',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['unique_id' => 'nim']);
    }
}
