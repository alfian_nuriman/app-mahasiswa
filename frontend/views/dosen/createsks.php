<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Matakuliah

/* @var $this yii\web\View */
/* @var $model common\models\Sks */
/* @var $form ActiveForm */
?>
<div class="dosen-createsks">

    <?php $form = ActiveForm::begin(); ?>

        <?=
            $form->field($model, 'id_matakuliah') 
             ->dropdownList(
                 ArrayHelper::map(Matakuliah::find()->all(), 'id_matakuliah', 'nama'),
                 ['prompt' => 'pilih mata kuliah']
             )->label(
                 'Mata kuliah',
                 ['class' => 'label-class']
             )
        ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- dosen-createsks -->