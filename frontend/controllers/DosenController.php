<?php

namespace frontend\controllers;

use Yii;
use common\models\Sks;
use common\models\Dosen;
use common\models\Mahasiswa;

class DosenController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreateSks()
    {
    	$model = new Sks();
    	$dosen = new Dosen();

    	if ($model->load(Yii::$app->request->post()) && $model->validate()) {
    		
    	} else {
    		return $this->render('createsks', [
    			'model' => $model
    		]);
    	}
    }

}
