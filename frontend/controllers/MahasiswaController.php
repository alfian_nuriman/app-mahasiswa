<?php

namespace frontend\controllers;

use Yii;
use common\models\Mahasiswa;
use common\models\Sks;

class MahasiswaController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSks()
    {

    	$model = new Sks();
    	$mahasiswa = new Mahasiswa();

    	if ($model->load(Yii::$app->request->post())) {
    		
    		if ($model->canRegister($model->hasLimit(Yii::$app->user->identity->nim) {

    			$model->id_sks = 'SKS' . $model->generateCode();
	    		$model->nim = Yii::$app->user->identity->nim;
	    		$model->id_matakuliah = 1;

	    		$model->save(false);
    		}

    	} else {
    		return $this->render('sks', ['model' => $model]);
    	}

    }

}
